(function () {
    'use strict';

    angular.module('app').controller('EditCtrl', Ctrl);

    function Ctrl($http, $location, $routeParams) {
        var vm = this;
        vm.firstname = '';
        vm.phone = '';
        $http.get('api/contacts/' + $routeParams.id).then(function (result) {
            vm.firstname = result.data.name;
            vm.phone = result.data.phone;
        });

        vm.editContact = function () {
            var identity = {
                _id: $routeParams.id,
                name: vm.firstname,
                phone: vm.phone
            };

            $http.put('api/contacts/' + $routeParams.id, identity).then(function (){
                $location.path('/search');
            });
        }
    }
})();
