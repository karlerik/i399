(function () {
    'use strict';

    angular.module('app').controller('SearchCtrl', Ctrl);

    function Ctrl($http, modalService) {
        var vm = this;
        vm.myVar = 1;
        vm.contactsAll = [];
        vm.contactsFiltered = [];
        vm.newItem = '';
        vm.search = '';

        vm.fetch = function () {
            $http.get('api/contacts').then(function (result) {
                vm.contactsAll = result.data;
                vm.contactsFiltered = result.data;
            });
        }
        vm.fetch();

        vm.remove = function (id){
            modalService.confirm().then(function (){
                return $http.delete('api/contacts/'+id);
            }).then( function (){ vm.fetch(); } );
        }

        vm.filterList = function (){
            vm.contactsFiltered = vm.contactsAll.filter(function(contact){
                if(contact.name.indexOf(vm.search) != -1 ||
                   contact.phone.indexOf(vm.search) != -1)
                {
                    return true;
                }
            }); 
        }
        
    }

})();