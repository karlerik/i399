(function () {
    'use strict';

    angular.module('app').controller('NewCtrl', Ctrl);

    function Ctrl($http, $location) {
        var vm = this;
        vm.firstname = "";
        vm.phone = "";

        vm.createContact = function () {
            var identity = {
                name: vm.firstname,
                phone: vm.phone
            };

            $http.post('api/contacts', identity).then(function (){
                $location.path('/search');
            });
        }
    }
})();